package list

import org.scalatest.FunSuite

import list.P01._
/**
  * Created by shiv on 8/7/16.
  */
class TestList extends FunSuite {

  test("test last element list in non empty list List(1,2,3) is 3") {
    assert(last(List(1,2,3)).getOrElse("Empty List") == 3)


  }

  test("test last element list non empty list List() is \"EmptyList\" ") {
    assert(last(List()).getOrElse("Empty List") == "Empty List")


  }


}
