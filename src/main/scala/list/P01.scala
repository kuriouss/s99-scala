package list

/**
  * Created by shiv on 8/7/16.
  */
object P01 {
  def last[T](list: List[T]):Option[T] = list match {
    case Nil => None
    case x::Nil => Some(x)
    case x::xs => last(xs)
  }

  def penultimate[T](list: List[T]):Option[T] = list match {
    case x::y::Nil => Some(x)
    case x::xs => penultimate(xs)
    case _ => None

  }

  def nth[T](n: Int, list: List[T]):Option[T] = (n,list) match {
    case (_,Nil) => None
    case (0,x::xs) => Some(x)
    case (n,x::xs) => nth(n-1,xs)
  }

  def main(args: Array[String]) {
    println(last(List(1,2,3)).getOrElse("Empty List"))
    println(last(List()).getOrElse("Empty List"))
  }
}
