name := "s99"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-reflect" % "2.11.8",
  "org.scalatest" % "scalatest_2.11" % "2.2.4",
  "junit" % "junit" % "4.10" ,
  "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.4"
)
